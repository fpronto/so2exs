//includes do exerc�cio anterior�
#include <windows.h>
#include <tchar.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#define LIM 10000
#define NUM 4

typedef struct {
	int x;
	int y;
	int start;
	int end;
}values;

DWORD WINAPI Thread(LPVOID param);
void gotoxy(int x, int y);
int _tmain(int argc, LPTSTR argv[]) {
	if (argc < 3) {
		return 0;
	}

	int num = _tcstol_l(argv[1], NULL, 10, NULL);
	int limite = _tcstol_l(argv[2], NULL, 10, NULL);

	TCHAR resp;
	int y, x;
	BOOLEAN flag = FALSE;
	DWORD *threadId = malloc(sizeof(DWORD) * num); //Id da thread a ser criada
	HANDLE *hT = malloc(sizeof(HANDLE) * num); //HANDLE/ponteiro para a thread a ser criada
	values *v = malloc(sizeof(values) * num);

#ifdef UNICODE
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
#endif

	srand((int)time(NULL));
	_tprintf(TEXT("Lan�ar threads (S/N)?"));
	_tscanf_s(TEXT("%c"), &resp, 1);

	if (resp == 'S' || resp == 's') {
		for (int i = 0; i < num; i++) {
			v[i].x = rand() % 40;
			v[i].y = rand() % 40;;
			v[i].start = i * (limite / num);
			v[i].end = (i * (limite / num)) + (limite / num);

			hT[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Thread, (LPVOID)&v[i], 0, &
				threadId[i]);

		}
		SetThreadPriority(hT[0], THREAD_PRIORITY_LOWEST);
		for (int i = 1; i < num; i++) {
			SetThreadPriority(hT[i], THREAD_PRIORITY_HIGHEST);
		}
		for (int i = 0; i < num; i++) {
			if (hT[i] == NULL) {
				flag = TRUE;
			}
		}
		if (!flag) {
			for (int i = 0; i < num; i++) {
				_tprintf(TEXT("Lancei uma thread com id %d\n"), threadId[i]);
			}
			WaitForMultipleObjects(num, hT, TRUE, INFINITE);

			free(threadId);
			free(hT);
			free(v);
		}
		else {
			_tprintf(TEXT("Ocorreu um erro"));
		}
	}
	_tprintf(TEXT("[Thread Principal %d]Vou terminar..."), GetCurrentThreadId());
	return 0;
}
/* ----------------------------------------------------- */
/* "Thread" - Funcao associada � Thread */
/* ----------------------------------------------------- */

DWORD WINAPI Thread(LPVOID param) {
	int i;
	values *v = ((values*)param);
	_tprintf(TEXT("[Thread %d]Vou come�ar a trabalhar\n"), GetCurrentThreadId());
	Sleep(100);

	for (i = v->start; i < v->end; i++) {
		gotoxy(v->x, v->y);
		_tprintf(TEXT("Thread %5d"), i);
		Sleep(10);
	}
	return 0;
}

void gotoxy(int x, int y) {
	static HANDLE hStdout = NULL;
	COORD coord;
	coord.X = x;
	coord.Y = y;
	if (!hStdout)
		hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hStdout, coord);
}

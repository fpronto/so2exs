//includes do exerc�cio anterior�
#include <windows.h>
#include <tchar.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#define LIM 10000
#define NUM 4

typedef struct {
	int start;
	int end;
}values;

TCHAR * word;
TCHAR * filename;

DWORD WINAPI Thread(LPVOID param);
void gotoxy(int x, int y);
int _tmain(int argc, LPTSTR argv[]) {
	//Program <NThreads> <Word> <FileName>
	if (argc < 4) {
		_tprintf(TEXT("Don't have all parameters"));

		return -1;
	}

	int num = _tcstol_l(argv[1], NULL, 10, NULL);

	word = argv[2];
	filename = argv[3];

	TCHAR resp;
	DWORD tickInit = GetTickCount();
	DWORD *threadId = malloc(sizeof(DWORD) * num); //Id da thread a ser criada
	HANDLE *hT = malloc(sizeof(HANDLE) * num); //HANDLE/ponteiro para a thread a ser criada
	values *v = malloc(sizeof(values) * num);
	FILE *file;

	if (_tfopen_s(&file, filename, TEXT("rt")) != 0) {
		_tprintf(TEXT("Can't open file"));
		return -1;
	}

		
	fseek(file, 0L, SEEK_END);
	
	int lim = ftell(file);
	int range = lim / num;

	fclose(file);
#ifdef UNICODE
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
#endif

	srand((int)time(NULL));
	_tprintf(TEXT("Lan�ar threads (S/N)?"));
	_tscanf_s(TEXT("%c"), &resp, 1);
	if (resp == 'S' || resp == 's') {

		for (int i = 0; i < num; i++) {
			v[i].start = i * (range);
			v[i].end = (i+1) * range;

			hT[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Thread, (LPVOID) &v[i], 0, &
				threadId[i]);
		}

		for (int i = 0; i < num; i++){
			_tprintf(TEXT("Lancei uma thread com id %d\n"), threadId[i]);
		}
		WaitForMultipleObjects(num, hT, TRUE, INFINITE);

	}
	_tprintf(TEXT("[Thread Principal %d]Vou terminar..."), GetCurrentThreadId());
	return 0;
}
/* ----------------------------------------------------- */
/* "Thread" - Funcao associada � Thread */
/* ----------------------------------------------------- */

DWORD WINAPI Thread(LPVOID param) {
	int i;
	FILE * f;
	TCHAR line[LIM];
	values *v = ((values*)param);
	_tprintf(TEXT("[Thread %d]Vou come�ar a trabalhar\n"), GetCurrentThreadId());
	_tprintf(TEXT("Values: start ->%d  end-> %d\n"), v->start, v->end);
	if (_tfopen_s(&f, filename, TEXT("rt")) != 0) {
		_tprintf(TEXT("Can't open file"));
		return -1;
	}
	fseek(f, v->start, SEEK_SET);

	while (_fgetts(&line, LIM, f) != NULL) {
		if (_tcsstr(&line, word) != NULL) {
			_tprintf(TEXT("[Thread %d]linha -> %s\n"), GetCurrentThreadId(), line);
		}
		if (ftell(f) > v->end) {
			break;
		}
	}
	return 0;
}
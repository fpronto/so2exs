//usaDLL.cpp
#include <windows.h>
#include <tchar.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>

int _tmain(int argc, TCHAR *argv[]) {
	HMODULE hLib;
	FARPROC varAdd;
	FARPROC procAdd;

	hLib = LoadLibrary("DLL1.dll");
	if (hLib != NULL) {

		varAdd = (FARPROC)GetProcAddress(hLib, "nDLL");

		procAdd = (FARPROC)GetProcAddress(hLib, "UmaString");

		if (varAdd != NULL && procAdd != NULL) {
			(*((int *)varAdd)) += 1;
			//Usar a vari�vel da Dll
			_tprintf(TEXT("Valor da vari�vel da DLL: %d\n"), *((int *)varAdd));
			//Chamar a funcao da Dll
			_tprintf(TEXT("Resultado da fun��o da UmaString DLL: %d"), procAdd());
		}

		FreeLibrary(hLib);
		_gettchar();
	}
	return 0;
}
